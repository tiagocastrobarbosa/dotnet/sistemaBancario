﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sistemaBancario.Model
{
    class Cliente
    {
        public string nome { get; set; }
        public long cpf { get; set; }
        public List<Conta> contas { get; set; }

        public Cliente(string nome, long cpf)
        {
            this.nome = nome;
            this.cpf = cpf;
            contas = new List<Conta>();
            Program.clientes.Add(this); // apenas porque não tô usando banco de dados :/
        }

        public double GetSaldo(Conta conta) // verificar se o método está sendo utilizado
        {
            return conta.GetSaldo();
        }

        public double GetInvestimentos()
        {
            if (contas.Count == 0)
            {
                return -1.0;
            }
            else
            {
                double total = 0.0;
                foreach (Conta conta in contas)
                {
                    total += conta.GetSaldo();
                }
                return total;
            }
        }
    }
}
