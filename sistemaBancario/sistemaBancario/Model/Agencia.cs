﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sistemaBancario.Model
{
    class Agencia : Operacao
    {
        public long codigo { get; set; }
        public Banco banco { get; set; }
        public double montante { get; private set; }

        public Agencia(long codigo, Banco banco)
        {
            this.codigo = codigo;
            this.banco = banco;
            montante = 0.0;
            Program.agencias.Add(this); // apenas porque não tô usando banco de dados :/
        }

        public override bool Depositar(double valor)
        {
            montante += valor;
            banco.Depositar(valor);
            return true;
        }

        public override bool Sacar(double valor)
        {
            montante -= valor;
            banco.Sacar(valor);
            return true;
        }

        public double GetMontante()
        {
            return montante;
        }
    }
}
