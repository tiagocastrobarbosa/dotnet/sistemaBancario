﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sistemaBancario.Model
{
    abstract class Operacao
    {
        public abstract bool Depositar(double valor);

        public abstract bool Sacar(double valor);
    }
}
