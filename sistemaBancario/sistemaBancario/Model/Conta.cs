﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sistemaBancario.Model
{
    class Conta : Operacao
    {
        public long codigo { get; set; }
        public double saldo { get; private set; }
        public Agencia agencia { get; set; }
        public Cliente cliente { get; set; }

        // ************************* SOBRECARGA *************************
        public Conta(long codigo, Agencia agencia, Cliente cliente)
        {
            this.codigo = codigo;
            this.agencia = agencia;
            this.cliente = cliente;
            saldo = 0.0;
            cliente.contas.Add(this);
            Program.contas.Add(this); // apenas porque não tô usando banco de dados :/
        }

        public Conta(long codigo, Agencia agencia, Cliente cliente, double saldo)
        {
            this.codigo = codigo;
            this.agencia = agencia;
            this.cliente = cliente;
            this.saldo = saldo;
            cliente.contas.Add(this);
            Program.contas.Add(this); // apenas porque não tô usando banco de dados :/
        }
        // **************************************************************

        public override bool Depositar(double valor)
        {
            saldo += valor;
            agencia.Depositar(valor);
            return true;
        }

        public override bool Sacar(double valor)
        {
            if (saldo <= 0.0)
            {
                return false;
            }

            saldo -= valor;
            agencia.Sacar(valor);
            return true;
        }

        public double GetSaldo()
        {
            return saldo;
        }

        public bool Transferir(double valor, Conta conta)
        {
            if (Sacar(valor))
            {
                conta.Depositar(valor);
                return true;
            }

            return false;
        }
    }
}
