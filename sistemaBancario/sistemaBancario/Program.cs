﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sistemaBancario.View;
using sistemaBancario.Model;

namespace sistemaBancario
{
    class Program
    {
        /* Instanciação das listas */
        public static List<Banco> bancos = new List<Banco>();
        public static List<Agencia> agencias = new List<Agencia>();
        public static List<Conta> contas = new List<Conta>();
        public static List<Cliente> clientes = new List<Cliente>();

        static void Main(string[] args)
        {
            // ********** Instanciação dos Objetos **********
            Banco banco1 = new Banco(427);
            Banco banco2 = new Banco(531);
            bancos.Add(banco1);
            bancos.Add(banco2);

            Agencia agencia1 = new Agencia(4423, banco1);
            Agencia agencia2 = new Agencia(5571, banco1);
            Agencia agencia3 = new Agencia(2208, banco2);
            agencias.Add(agencia1);
            agencias.Add(agencia2);
            agencias.Add(agencia3);

            Cliente cliente1 = new Cliente("Tiago", 8584967);
            clientes.Add(cliente1);
            Cliente cliente2 = new Cliente("Larissa", 7694858);
            clientes.Add(cliente2);

            Conta conta1 = new Conta(7777, agencia1, cliente1);
            contas.Add(conta1);
            Conta conta2 = new Conta(8888, agencia2, cliente1);
            contas.Add(conta2);
            Conta conta3 = new Conta(9999, agencia3, cliente2);
            contas.Add(conta3);
            // **********************************************

            // Chama o menu principal
            Tela.Home();
        }
    }
}
