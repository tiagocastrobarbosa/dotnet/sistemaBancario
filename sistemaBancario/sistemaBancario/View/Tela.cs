﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using sistemaBancario.Model;
using System.Globalization;

namespace sistemaBancario.View
{
    static class Tela
    {
        public static void Home()
        {
            bool exitMenu = false;
            while (exitMenu != true)
            {
                Console.Clear();
                Console.WriteLine("Selecione a área:\n" +
                "1 - Área do cliente\n" +
                "2 - Área da agência\n" +
                "3 - Área do banco\n" +
                "4 - Sair");
                Console.Write(">>> ");
                try
                {
                    int n = int.Parse(Console.ReadLine());
                    if (n == 1)
                    {
                        AreaCliente();
                    }

                    else if (n == 2)
                    {
                        AreaAgencia();
                    }

                    else if (n == 3)
                    {
                        AreaBanco();
                    }

                    else if (n == 4)
                    {
                        Console.WriteLine();
                        Console.WriteLine("Programa finalizado.");
                        System.Threading.Thread.Sleep(2000);
                        exitMenu = true;
                    }

                    else
                    {
                        throw new ModelException("O valor digitado não " +
                            "corresponde aos itens de menu.");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Erro inesperado: " + e.Message);
                    Console.ReadKey();
                }
            }
        }

        public static void AreaCliente()
        {
            bool exitMenu = false;
            while (exitMenu != true)
            {
                Console.Clear();
                Console.WriteLine("Selecione a opção desejada:\n" +
                    "1 - Cadastrar novo cliente\n" +
                    "2 - Cadastrar nova conta\n" +
                    "3 - Solicitar saldo\n" +
                    "4 - Solicitar todos os saldos\n" +
                    "5 - Depositar\n" +
                    "6 - Sacar\n" +
                    "7 - Transferir\n" +
                    "8 - Listar todas as contas\n" +
                    "9 - Voltar");
                Console.Write(">>> ");
                try
                {
                    int n = int.Parse(Console.ReadLine());
                    if (n == 1)
                    {
                        // criar essa tela
                    }

                    else if (n == 2)
                    {
                        CadastroConta();
                    }

                    else if (n == 3)
                    {
                        Saldo();
                    }

                    else if (n == 4)
                    {
                        TodosSaldos();
                    }

                    else if (n == 5)
                    {
                        Deposito();
                    }

                    else if (n == 6)
                    {
                        Saque();
                    }

                    else if (n == 7)
                    {
                        Transferencia();
                    }

                    else if (n == 8)
                    {
                        ListarContas();
                    }

                    else if (n == 9)
                    {
                        exitMenu = true;
                    }

                    else
                    {
                        throw new ModelException("O valor digitado não " +
                            "corresponde aos itens de menu.");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Erro inesperado: " + e.Message);
                    Console.ReadKey();
                }
            }
        }

        public static void AreaAgencia()
        {
            bool exitMenu = false;
            while (exitMenu != true)
            {
                Console.Clear();
                Console.WriteLine("Selecione a opção desejada:\n" +
                    "1 - Cadastrar nova agência\n" +
                    "2 - Mostrar montante\n" +
                    "3 - Listar todas as agências\n" +
                    "4 - Voltar");
                Console.Write(">>> ");
                try
                {
                    int n = int.Parse(Console.ReadLine());
                    if (n == 1)
                    {
                        CadastrarAgencia();
                    }

                    else if (n == 2)
                    {
                        MontanteAgencia();
                    }

                    else if (n == 3)
                    {
                        ListarAgencias();
                    }

                    else if (n == 4)
                    {
                        exitMenu = true;
                    }

                    else
                    {
                        throw new ModelException("O valor digitado não " +
                            "corresponde aos itens de menu.");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Erro inesperado: " + e.Message);
                    Console.ReadKey();
                }
            }
        }

        public static void AreaBanco()
        {
            bool exitMenu = false;
            while (exitMenu != true)
            {
                Console.Clear();
                Console.WriteLine("Selecione a opção desejada:\n" +
                    "1 - Cadastrar novo banco\n" +
                    "2 - Mostrar montante do banco\n" +
                    "3 - Listar todos os bancos\n" +
                    "4 - Voltar");
                Console.Write(">>> ");
                try
                {
                    int n = int.Parse(Console.ReadLine());
                    if (n == 1)
                    {
                        CadastroBanco();
                    }

                    else if (n == 2)
                    {
                        MontanteBanco();
                    }

                    else if (n == 3)
                    {
                        ListarBancos();
                    }

                    else if (n == 4)
                    {
                        exitMenu = true;
                    }

                    else
                    {
                        throw new ModelException("O valor digitado não " +
                            "corresponde aos itens de menu.");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Erro inesperado: " + e.Message);
                    Console.ReadKey();
                }
            }
        }

        public static void CadastroConta()
        {
            bool exitMenu = false;
            while (exitMenu != true)
            {
                Console.Clear();
                Console.WriteLine("Digite 0 (zero) caso deseje voltar " +
                    "para o menu anterior.");
                Console.Write("CPF do cliente: ");
                try
                {
                    long cpf = long.Parse(Console.ReadLine());
                    if (cpf == 0)
                    {
                        exitMenu = true;
                    }

                    // verifica se já não existe um cpf igual cadastrado no sistema
                    else if (cpf > 0 && Program.clientes.Find(x => x.cpf == cpf) != null)
                    {
                        Cliente cliente = Program.clientes.Find(x => x.cpf == cpf);
                        Console.Write("Código da conta: ");
                        long codigo = long.Parse(Console.ReadLine());
                        if (codigo == 0)
                        {
                            exitMenu = true;
                        }

                        // verifica se já não existe um código de conta igual cadastrado no sistema
                        else if (codigo > 0 && Program.contas.Find(x => x.codigo == codigo) == null)
                        {
                            Console.Write("Código da agência: ");
                            long codigoAgencia = long.Parse(Console.ReadLine());
                            if (codigoAgencia == 0)
                            {
                                exitMenu = true;
                            }

                            // verifica se já existe um código de agência cadastrado no sistema
                            else if (codigoAgencia > 0 && Program.agencias.Find(x => x.codigo == codigoAgencia) != null)
                            {
                                Agencia agencia = Program.agencias.Find(x => x.codigo == codigoAgencia);
                                Console.WriteLine("A conta terá saldo inicial? (y/n)");
                                char awnser = char.Parse(Console.ReadLine());
                                if (awnser == 'y')
                                {
                                    Console.Write("Valor do depósito inicial: ");
                                    double valor = double.Parse(Console.ReadLine());
                                    if (valor == 0)
                                    {
                                        exitMenu = true;
                                    }

                                    // cadastra uma conta com saldo inicial
                                    else if (valor > 0)
                                    {
                                        Conta conta = new Conta(codigo, agencia, cliente, valor);
                                    }

                                    else
                                    {
                                        throw new ModelException("Por favor digite um dado válido.");
                                    }
                                }

                                // cadastra uma conta sem saldo inicial
                                else
                                {
                                    Conta conta = new Conta(codigo, agencia, cliente);
                                }

                                Console.WriteLine();
                                Console.WriteLine("Conta cadastrada com sucesso!");
                                Console.WriteLine();
                                Console.WriteLine("Pressione qualquer tecla para voltar.");
                                Console.ReadKey();
                                exitMenu = true;
                            }

                            else
                            {
                                throw new ModelException("Por favor digite um dado válido.");
                            }
                        }

                        else
                        {
                            throw new ModelException("Por favor digite um dado válido.");
                        }
                    }

                    else
                    {
                        throw new ModelException("Por favor digite um dado válido.");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Erro inesperado: " + e.Message);
                    Console.ReadKey();
                }
            }
        }

        public static void Saldo()
        {
            bool exitMenu = false;
            while (exitMenu != true)
            {
                Console.Clear();
                Console.WriteLine("Digite 0 (zero) caso deseje voltar " +
                        "para o menu anterior.");
                Console.Write("Código da conta: ");
                try
                {
                    long codigo = long.Parse(Console.ReadLine());
                    if (codigo == 0)
                    {
                        exitMenu = true;
                    }

                    // Verifica se existe essa conta digitada
                    else if (codigo > 0 && Program.contas.Find(x => x.codigo == codigo) != null)
                    {
                        Conta conta = Program.contas.Find(x => x.codigo == codigo);
                        double saldo = conta.GetSaldo();
                        Console.WriteLine("\tConta {0}: saldo de R$ {1}",
                            conta.codigo.ToString(), saldo.ToString("F2", CultureInfo.InvariantCulture));
                        Console.WriteLine();
                        Console.WriteLine("Pressione qualquer tecla para voltar.");
                        Console.ReadKey();
                        exitMenu = true;
                    }

                    else
                    {
                        throw new ModelException("Por favor digite um dado válido.");
                    }
                }

                catch (Exception e)
                {
                    Console.WriteLine("Erro inesperado: " + e.Message);
                    Console.ReadKey();
                }
            }
        }

        public static void TodosSaldos()
        {
            bool exitMenu = false;
            while (exitMenu != true)
            {
                Console.Clear();
                Console.WriteLine("Digite 0 (zero) caso deseje voltar " +
                        "para o menu anterior.");
                Console.Write("CPF do cliente: ");
                try
                {
                    long cpf = long.Parse(Console.ReadLine());
                    if (cpf == 0)
                    {
                        exitMenu = true;
                    }

                    // Verifica se existe esse cpf digitado
                    else if (cpf > 0 && Program.clientes.Find(x => x.cpf == cpf) != null)
                    {
                        Cliente cliente = Program.clientes.Find(x => x.cpf == cpf);
                        if (cliente.contas.Count > 0)
                        {
                            foreach (var conta in cliente.contas)
                            {
                                Console.WriteLine("\tConta {0}: saldo de R$ {1}",
                                conta.codigo.ToString(), conta.GetSaldo().ToString("F2", CultureInfo.InvariantCulture));
                            }
                            Console.WriteLine();
                            Console.WriteLine("Pressione qualquer tecla para voltar.");
                            Console.ReadKey();
                            exitMenu = true;
                        }

                        else
                        {
                            Console.WriteLine("Cliente não tem conta cadastrada ainda!");
                            Console.WriteLine();
                            Console.WriteLine("Pressione qualquer tecla para voltar.");
                            Console.ReadKey();
                            exitMenu = true;
                        }
                    }

                    else
                    {
                        throw new ModelException("Por favor digite um dado válido.");
                    }
                }

                catch (Exception e)
                {
                    Console.WriteLine("Erro inesperado: " + e.Message);
                    Console.ReadKey();
                }
            }
        }

        public static void Deposito()
        {
            bool exitMenu = false;
            while (exitMenu != true)
            {
                Console.Clear();
                Console.WriteLine("Digite 0 (zero) caso deseje voltar " +
                        "para o menu anterior.");
                Console.Write("Código da conta: ");
                try
                {
                    long codigo = long.Parse(Console.ReadLine());
                    if (codigo == 0)
                    {
                        exitMenu = true;
                    }

                    // Verifica se existe essa conta digitada
                    else if (codigo > 0 && Program.contas.Find(x => x.codigo == codigo) != null)
                    {
                        Console.Write("Valor do depósito: ");
                        double valor = double.Parse(Console.ReadLine());
                        Conta conta = Program.contas.Find(x => x.codigo == codigo);
                        if (conta.Depositar(valor))
                        {
                            Console.WriteLine("Valor depositado com sucesso!");
                            Console.WriteLine();
                            Console.WriteLine("Pressione qualquer tecla para voltar.");
                            Console.ReadKey();
                            exitMenu = true;
                        }

                        else
                        {
                            Console.WriteLine("Falha no depósito!");
                            Console.WriteLine();
                            Console.WriteLine("Pressione qualquer tecla para voltar.");
                            Console.ReadKey();
                            exitMenu = true;
                        }
                    }

                    else
                    {
                        throw new ModelException("Por favor digite um dado válido.");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Erro inesperado: " + e.Message);
                    Console.ReadKey();
                }
            }
        }

        public static void Saque()
        {
            bool exitMenu = false;
            while (exitMenu != true)
            {
                Console.Clear();
                Console.WriteLine("Digite 0 (zero) caso deseje voltar " +
                        "para o menu anterior.");
                Console.Write("Código da conta: ");
                try
                {
                    long codigo = long.Parse(Console.ReadLine());
                    if (codigo == 0)
                    {
                        exitMenu = true;
                    }

                    // Verifica se existe essa conta digitada
                    else if (codigo > 0 && Program.contas.Find(x => x.codigo == codigo) != null)
                    {
                        Console.Write("Valor de saque: ");
                        double valor = double.Parse(Console.ReadLine());
                        Conta conta = Program.contas.Find(x => x.codigo == codigo);
                        if (conta.Sacar(valor))
                        {
                            Console.WriteLine("Valor sacado com sucesso!");
                            Console.WriteLine();
                            Console.WriteLine("Pressione qualquer tecla para voltar.");
                            Console.ReadKey();
                            exitMenu = true;
                        }

                        else
                        {
                            Console.WriteLine("Falha no saque!");
                            Console.WriteLine();
                            Console.WriteLine("Pressione qualquer tecla para voltar.");
                            Console.ReadKey();
                            exitMenu = true;
                        }
                    }

                    else
                    {
                        throw new ModelException("Por favor digite um dado válido.");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Erro inesperado: " + e.Message);
                    Console.ReadKey();
                }
            }
        }

        public static void Transferencia()
        {
            bool exitMenu = false;
            while (exitMenu != true)
            {
                Console.Clear();
                Console.WriteLine("Digite 0 (zero) caso deseje voltar " +
                        "para o menu anterior.");
                Console.Write("Código da sua conta: ");
                try
                {
                    long codigo = long.Parse(Console.ReadLine());
                    if (codigo == 0)
                    {
                        exitMenu = true;
                    }

                    // Verifica se existe essa conta digitada
                    else if (codigo > 0 && Program.contas.Find(x => x.codigo == codigo) != null)
                    {
                        Console.Write("Código da conta para envio: ");
                        long codigoEnvio = long.Parse(Console.ReadLine());
                        if (codigoEnvio == 0)
                        {
                            exitMenu = true;
                        }

                        // Verifica se existe essa conta digitada
                        else if (codigoEnvio > 0 && Program.contas.Find(x => x.codigo == codigoEnvio) != null)
                        {
                            Console.Write("Valor de transferência: ");
                            double valor = double.Parse(Console.ReadLine());
                            Conta conta = Program.contas.Find(x => x.codigo == codigo);
                            Conta segundaConta = Program.contas.Find(x => x.codigo == codigoEnvio);
                            if (conta.Transferir(valor, segundaConta))
                            {
                                Console.WriteLine("Valor transferido com sucesso!");
                                Console.WriteLine();
                                Console.WriteLine("Pressione qualquer tecla para voltar.");
                                Console.ReadKey();
                                exitMenu = true;
                            }

                            else
                            {
                                Console.WriteLine("Falha na transferência!");
                                Console.WriteLine();
                                Console.WriteLine("Pressione qualquer tecla para voltar.");
                                Console.ReadKey();
                                exitMenu = true;
                            }
                        }

                        else
                        {
                            throw new ModelException("Por favor digite um dado válido.");
                        }
                    }

                    else
                    {
                        throw new ModelException("Por favor digite um dado válido.");
                    }
                }

                catch (Exception e)
                {
                    Console.WriteLine("Erro inesperado: " + e.Message);
                    Console.ReadKey();
                }
            }
        }

        public static void ListarContas()
        {
            if (Program.clientes.Count() > 0)
            {
                Console.Clear();
                Console.WriteLine("Listagem de todos clientes e contas cadastrados no sistema:");
                int qntd = 0;
                foreach (Cliente cliente in Program.clientes)
                {
                    foreach (Conta conta in cliente.contas)
                    {
                        qntd++;
                        Console.WriteLine("\t{0} -> Conta {1} (Cliente {2})", qntd, conta.codigo, cliente.cpf);
                        //Console.WriteLine("\tCliente {0} - conta {1}", cliente.cpf, conta.codigo);
                    }
                }
                Console.WriteLine();
                Console.WriteLine("Pressione qualquer tecla para voltar.");
                Console.ReadKey();
            }

            // A lista está vazia sem nenhum cliente cadastrado
            else
            {
                Console.Clear();
                Console.WriteLine("Não existe nenhum cliente cadastrado no sistema ainda.");
                Console.WriteLine();
                Console.WriteLine("Pressione qualquer tecla para voltar.");
                Console.ReadKey();
            }
        }

        public static void ListarAgencias()
        {
            if (Program.agencias.Count() > 0)
            {
                Console.Clear();
                Console.WriteLine("Listagem de todas agências cadastradas no sistema:");
                int qntd = 0;
                foreach (Agencia agencia in Program.agencias)
                {
                    qntd++;
                    Console.WriteLine("\t{0} -> Agência {1} (Banco {2})", qntd,
                        agencia.codigo, agencia.banco.codigo);
                }
                Console.WriteLine();
                Console.WriteLine("Pressione qualquer tecla para voltar.");
                Console.ReadKey();
            }

            // A lista está vazia sem nenhuma agência cadastrada
            else
            {
                Console.Clear();
                Console.WriteLine("Não existe nenhuma agência cadastrada no sistema ainda.");
                Console.WriteLine();
                Console.WriteLine("Pressione qualquer tecla para voltar.");
                Console.ReadKey();
            }
        }

        public static void CadastrarAgencia()
        {
            bool exitMenu = false;
            while (exitMenu != true)
            {
                Console.Clear();
                Console.WriteLine("Digite 0 (zero) caso deseje voltar " +
                    "para o menu anterior.");
                Console.Write("Código da agência: ");
                try
                {
                    long codigo = long.Parse(Console.ReadLine());
                    if (codigo == 0)
                    {
                        exitMenu = true;
                    }

                    // verifica se o código é positivo e se a agência já existe
                    else if (codigo > 0 && Program.agencias.Find(x => x.codigo == codigo) == null)
                    {
                        Console.Write("Código do banco: ");
                        long codigoBanco = long.Parse(Console.ReadLine());
                        if (codigoBanco == 0)
                        {
                            exitMenu = true;
                        }

                        // verifica se o código é positivo e se o banco já existe
                        else if (codigoBanco > 0 && Program.bancos.Find(x => x.codigo == codigoBanco) != null)
                        {
                            Banco banco = Program.bancos.Find(x => x.codigo == codigoBanco);
                            Agencia agencia = new Agencia(codigo, banco);
                            Console.WriteLine("Agência cadastrada com sucesso!");
                            Console.WriteLine();
                            Console.WriteLine("Pressione qualquer tecla para voltar.");
                            Console.ReadKey();
                            exitMenu = true;
                        }

                        else
                        {
                            throw new ModelException("Por favor digite um dado válido.");
                        }
                    }

                    else
                    {
                        throw new ModelException("Por favor digite um dado válido.");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Erro inesperado: " + e.Message);
                    Console.ReadKey();
                }
            }
        }

        public static void MontanteAgencia()
        {
            bool exitMenu = false;
            while (exitMenu != true)
            {
                Console.Clear();
                Console.WriteLine("Digite 0 (zero) caso deseje voltar " +
                    "para o menu anterior.");
                Console.Write("Código da agência: ");
                try
                {
                    long codigo = long.Parse(Console.ReadLine());
                    if (codigo == 0)
                    {
                        exitMenu = true;
                    }

                    // verifica se o código é positivo e se o banco já existe
                    else if (codigo > 0 && Program.agencias.Find(x => x.codigo == codigo) != null)
                    {
                        Agencia agencia = Program.agencias.Find(x => x.codigo == codigo); // melhorar essa linha. Ele tá fazendo a pesquisa 2 vezes
                        Console.WriteLine("A agência {0} tem o montante investido " +
                            "equivalente a R$ {1}.", agencia.codigo,
                            agencia.GetMontante().ToString("F2", CultureInfo.InvariantCulture));
                        Console.WriteLine();
                        Console.WriteLine("Pressione qualquer tecla para voltar.");
                        Console.ReadKey();
                        exitMenu = true;
                    }

                    else
                    {
                        throw new ModelException("Por favor digite um dado válido.");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Erro inesperado: " + e.Message);
                    Console.ReadKey();
                }
            }
        }

        public static void CadastroBanco()
        {
            bool exitMenu = false;
            while (exitMenu != true)
            {
                Console.Clear();
                Console.WriteLine("Digite 0 (zero) caso deseje voltar " +
                    "para o menu anterior.");
                Console.Write("Código do banco: ");
                try
                {
                    long codigo = long.Parse(Console.ReadLine());
                    if (codigo == 0)
                    {
                        exitMenu = true;
                    }

                    // verifica se o código é positivo e se o banco já existe
                    else if (codigo > 0 && Program.bancos.Find(x => x.codigo == codigo) == null)
                    {
                        Banco banco = new Banco(codigo);
                        Console.WriteLine("Banco cadastrado com sucesso!");
                        Console.WriteLine();
                        Console.WriteLine("Pressione qualquer tecla para voltar.");
                        Console.ReadKey();
                        exitMenu = true;
                    }

                    else
                    {
                        throw new ModelException("Por favor digite um dado válido.");
                    }
                }

                catch (Exception e)
                {
                    Console.WriteLine("Erro inesperado: " + e.Message);
                    Console.ReadKey();
                }
            }
        }

        public static void MontanteBanco()
        {
            bool exitMenu = false;
            while (exitMenu != true)
            {
                Console.Clear();
                Console.WriteLine("Digite 0 (zero) caso deseje voltar " +
                    "para o menu anterior.");
                Console.Write("Código do banco: ");
                try
                {
                    long codigo = long.Parse(Console.ReadLine());
                    if (codigo == 0)
                    {
                        exitMenu = true;
                    }

                    // verifica se o código é positivo e se o banco já existe
                    else if (codigo > 0 && Program.bancos.Find(x => x.codigo == codigo) != null)
                    {
                        Banco banco = Program.bancos.Find(x => x.codigo == codigo); // melhorar essa linha. Ele tá fazendo a pesquisa 2 vezes
                        Console.WriteLine("O banco {0} tem o montante investido " +
                            "equivalente a R$ {1}.", banco.codigo,
                            banco.GetMontante().ToString("F2", CultureInfo.InvariantCulture));
                        Console.WriteLine();
                        Console.WriteLine("Pressione qualquer tecla para voltar.");
                        Console.ReadKey();
                        exitMenu = true;
                    }

                    else
                    {
                        throw new ModelException("Por favor digite um dado válido.");
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("Erro inesperado: " + e.Message);
                    Console.ReadKey();
                }
            }
        }

        public static void ListarBancos()
        {
            if (Program.bancos.Count() > 0)
            {
                Console.Clear();
                Console.WriteLine("Listagem de todos bancos cadastrados no sistema:");
                int qntd = 0;
                foreach (Banco banco in Program.bancos)
                {
                    qntd++;
                    Console.WriteLine("\t{0} -> {1}", qntd, banco.codigo);
                }
                Console.WriteLine();
                Console.WriteLine("Pressione qualquer tecla para voltar.");
                Console.ReadKey();
            }

            // A lista está vazia sem nenhum banco cadastrado
            else
            {
                Console.Clear();
                Console.WriteLine("Não existe nenhum banco cadastrado no sistema ainda.");
                Console.WriteLine();
                Console.WriteLine("Pressione qualquer tecla para voltar.");
                Console.ReadKey();
            }
        }
    }
}
